import game.Cache;
import game.GamePlay;

import javax.swing.*;
import java.awt.*;

public class Main {
    // framerate
    public static int ticksPerSecond;
    private static int skip;
    private static int max;
    private static double next_game_tick;

    public static void main(String[] args) throws InterruptedException
    {
        JFrame fenetre = new JFrame();
        GamePlay gamePlay = new GamePlay();
        Image icon = Toolkit.getDefaultToolkit().getImage("logo.png");

        fenetre.setBounds(5, 5, 512, 621);

        Cache.init();
        Cache.fenetre = fenetre;

        fenetre.setTitle("Chess");
        fenetre.setIconImage(icon);
        fenetre.setResizable(false);
        fenetre.setVisible(true);
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fenetre.add(gamePlay);

        gamePlay.newBoard();

        setFramerate(60);
        int loops;


        while(Cache.run)
        {
            loops = 0;
            while (System.currentTimeMillis() > next_game_tick && loops < max)
            {
                gamePlay.repaint();

                next_game_tick += skip;
                loops++;
            }
        }
        fenetre.dispose();
    }

    public static void setFramerate(int tps)
    {
        ticksPerSecond = tps;
        skip = 1000 / ticksPerSecond;
        max = 5;
        next_game_tick = System.currentTimeMillis();
    }

}
