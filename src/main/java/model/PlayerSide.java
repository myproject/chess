package model;

import com.sun.tools.javac.Main;

import java.awt.*;

public enum PlayerSide {
    black(Color.black),
    white(Color.white);

    private Color color;
    PlayerSide(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
