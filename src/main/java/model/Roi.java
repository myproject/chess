package model;

import game.Board;
import game.Cache;

import java.awt.*;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class Roi extends Piece {
    public Roi(PlayerSide playerSide) {
        super(playerSide);
    }

    @Override
    public Image getImage() {
        return playerSide == PlayerSide.white ? Cache.roiBlanc : Cache.roiNoir;
    }

    @Override
    public ArrayList<Case> getPotentialMove(Case currentCase, Board board) {
        ArrayList<Case> allPotentialCase = new ArrayList<>();

        allPotentialCase.add(board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() + 1, currentCase.getPosition().getIntY())));
        allPotentialCase.add(board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() - 1, currentCase.getPosition().getIntY())));
        allPotentialCase.add(board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX(), currentCase.getPosition().getIntY() + 1)));
        allPotentialCase.add(board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX(), currentCase.getPosition().getIntY() - 1)));
        allPotentialCase.add(board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() + 1, currentCase.getPosition().getIntY() - 1)));
        allPotentialCase.add(board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() + 1, currentCase.getPosition().getIntY() + 1)));
        allPotentialCase.add(board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() - 1, currentCase.getPosition().getIntY() - 1)));
        allPotentialCase.add(board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() - 1, currentCase.getPosition().getIntY() + 1)));

        return allPotentialCase
                .stream()
                .filter(aCase -> aCase != null && (aCase.getPiece() == null || aCase.getPiece().getPlayerSide() != this.playerSide))
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
