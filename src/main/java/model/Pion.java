package model;

import game.Board;
import game.Cache;

import java.awt.*;
import java.util.ArrayList;

public class Pion extends Piece {
    private boolean hasPlay = false;
    private boolean canPriseEnPassant = false;

    public Pion(PlayerSide playerSide) {
        super(playerSide);
    }

    @Override
    public Image getImage() {
        return playerSide == PlayerSide.white ? Cache.pionBlanc : Cache.pionNoir;
    }

    @Override
    public ArrayList<Case> getPotentialMove(Case currentCase, Board board) {
        ArrayList<Case> potentialMove = new ArrayList<>();

        Case front;
        Case frontDouble;
        Case left;
        Case right;
        Case leftPriseEnPassant;
        Case rightPriseEnPassant;
        if(playerSide == PlayerSide.black) {
            front = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX(), currentCase.getPosition().getIntY() + 1));
            frontDouble = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX(), currentCase.getPosition().getIntY() + 2));
            left = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() - 1, currentCase.getPosition().getIntY() + 1));
            right = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() + 1, currentCase.getPosition().getIntY() + 1));
            leftPriseEnPassant = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() - 1, currentCase.getPosition().getIntY()));
            rightPriseEnPassant = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() + 1, currentCase.getPosition().getIntY()));
        } else {
            front = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX(), currentCase.getPosition().getIntY() - 1));
            frontDouble = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX(), currentCase.getPosition().getIntY() - 2));
            left = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() - 1, currentCase.getPosition().getIntY() - 1));
            right = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() + 1, currentCase.getPosition().getIntY() - 1));
            leftPriseEnPassant = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() - 1, currentCase.getPosition().getIntY()));
            rightPriseEnPassant = board.getCaseAtCoordinate(new Vector(currentCase.getPosition().getIntX() + 1, currentCase.getPosition().getIntY()));
        }


        if(front != null && front.getPiece() == null) {
            potentialMove.add(front);
        }

        if(frontDouble != null && frontDouble.getPiece() == null && !hasPlay) {
            potentialMove.add(frontDouble);
        }

        if(left != null && left.getPiece() != null && left.getPiece().getPlayerSide() != this.playerSide) {
            potentialMove.add(left);
        }

        if(right != null && right.getPiece() != null && right.getPiece().getPlayerSide() != this.playerSide) {
            potentialMove.add(right);
        }

        if(leftPriseEnPassant != null && leftPriseEnPassant.getPiece() != null && leftPriseEnPassant.getPiece() instanceof Pion
                && leftPriseEnPassant.getPiece().getPlayerSide() != this.getPlayerSide() && ((Pion) leftPriseEnPassant.getPiece()).canPriseEnPassant
                && left != null) {
            potentialMove.add(left);
        }

        if(rightPriseEnPassant != null && rightPriseEnPassant.getPiece() != null && rightPriseEnPassant.getPiece() instanceof Pion
                && rightPriseEnPassant.getPiece().getPlayerSide() != this.getPlayerSide() && ((Pion) rightPriseEnPassant.getPiece()).canPriseEnPassant
                && right != null) {
            potentialMove.add(right);
        }

        return potentialMove;
    }

    public void play(boolean canPriseEnPassant, Case current, Case future, Board board) {
        hasPlay = true;
        this.canPriseEnPassant = canPriseEnPassant;

        // if player take in passing remove piece from left or right cases
        Case leftPriseEnPassant;
        Case rightPriseEnPassant;
        if(playerSide == PlayerSide.black) {
            leftPriseEnPassant = board.getCaseAtCoordinate(new Vector(current.getPosition().getIntX() - 1, current.getPosition().getIntY()));
            rightPriseEnPassant = board.getCaseAtCoordinate(new Vector(current.getPosition().getIntX() + 1, current.getPosition().getIntY()));
        } else {
            leftPriseEnPassant = board.getCaseAtCoordinate(new Vector(current.getPosition().getIntX() - 1, current.getPosition().getIntY()));
            rightPriseEnPassant = board.getCaseAtCoordinate(new Vector(current.getPosition().getIntX() + 1, current.getPosition().getIntY()));
        }

        if(leftPriseEnPassant != null && leftPriseEnPassant.getPiece() != null && leftPriseEnPassant.getPiece() instanceof Pion
                && leftPriseEnPassant.getPiece().getPlayerSide() != this.getPlayerSide() && ((Pion) leftPriseEnPassant.getPiece()).canPriseEnPassant) {
            leftPriseEnPassant.setPiece(null);
        } else if(rightPriseEnPassant != null && rightPriseEnPassant.getPiece() != null && rightPriseEnPassant.getPiece() instanceof Pion
                && rightPriseEnPassant.getPiece().getPlayerSide() != this.getPlayerSide() && ((Pion) rightPriseEnPassant.getPiece()).canPriseEnPassant) {
            rightPriseEnPassant.setPiece(null);
        }
    }

    public void setCanPriseEnPassant(boolean canPriseEnPassant) {
        this.canPriseEnPassant = canPriseEnPassant;
    }
}
