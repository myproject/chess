package model;

/**
 * class Vector
 */
public class Vector
{
    public double x;
    public double y;

    /**
     * Vector constructor with int
     * @param x
     * @param y
     */
    public Vector(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * Vector constructor with double
     * @param x
     * @param y
     */
    public Vector(Double x, Double y)
    {
        this.x = x;
        this.y = y;
    }


    /**
     * getter on x
     * @return
     */
    public double getX()
    {
        return x;
    }

    /**
     * getter on x (int)
     * @return
     */
    public int getIntX()
    {
        return (int) x;
    }

    /**
     * setter on x
     * @param x
     */
    public void setX(double x)
    {
        this.x = x;
    }

    /**
     * getter on y
     * @return
     */
    public double getY()
    {
        return y;
    }

    /**
     * getter on y (int)
     * @return
     */
    public int getIntY()
    {
        return (int) y;
    }

    /**
     * getter on y
     * @param y
     */
    public void setY(double y)
    {
        this.y = y;
    }

    /**
     * add value to x and y by value
     * @param x
     * @param y
     */
    public void add(double x, double y)
    {
        this.x += x;
        this.y += y;
    }

    /**
     * add value to x and y by vector
     * @param newVector
     */
    public void add(Vector newVector)
    {
        this.x += newVector.x;
        this.y += newVector.y;
    }

    @Override
    public String toString() {
        return "Vector{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
