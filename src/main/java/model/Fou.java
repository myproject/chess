package model;

import game.Board;
import game.Cache;

import java.awt.*;
import java.util.ArrayList;

public class Fou extends Piece {
    public Fou(PlayerSide playerSide) {
        super(playerSide);
    }

    @Override
    public Image getImage() {
        return playerSide == PlayerSide.white ? Cache.fouBlanc : Cache.fouNoir;
    }

    @Override
    public ArrayList<Case> getPotentialMove(Case currentCase, Board board) {
        ArrayList<Case> potentialMove = new ArrayList<>();

        int x = currentCase.getPosition().getIntX() - 1;
        int y = currentCase.getPosition().getIntY() - 1;
        boolean canMove = true;
        while(y >= 0 && x >= 0 && canMove) {
            canMove = addPotentialmove(board, potentialMove, x, y);
            x -= 1;
            y -= 1;
        }

        x = currentCase.getPosition().getIntX() - 1;
        y = currentCase.getPosition().getIntY() + 1;
        canMove = true;
        while(y < 8 && x >= 0 && canMove) {
            canMove = addPotentialmove(board, potentialMove, x, y);
            x -= 1;
            y += 1;
        }

        x = currentCase.getPosition().getIntX() + 1;
        y = currentCase.getPosition().getIntY() + 1;
        canMove = true;
        while(y < 8 && x < 8 && canMove) {
            canMove = addPotentialmove(board, potentialMove, x, y);
            x += 1;
            y += 1;
        }

        x = currentCase.getPosition().getIntX() + 1;
        y = currentCase.getPosition().getIntY() - 1;
        canMove = true;
        while(x < 8 && y >= 0 && canMove) {
            canMove = addPotentialmove(board, potentialMove, x, y);
            x += 1;
            y -= 1;
        }

        return potentialMove;
    }

    private boolean addPotentialmove(Board board, ArrayList<Case> potentialMove, int x, int y) {
            boolean canMove = true;
            Case aCase = board.getCaseAtCoordinate(new Vector(x, y));
            if(aCase != null) {
                if(aCase.getPiece() == null) {
                    potentialMove.add(aCase);
                } else {
                    if(aCase.getPiece().getPlayerSide() != this.playerSide) {
                        potentialMove.add(aCase);
                    }
                    canMove = false;
                }
            }
        return canMove;
    }
}
