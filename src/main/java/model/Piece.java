package model;

import game.Board;

import java.awt.*;
import java.util.ArrayList;

public abstract class Piece {
    PlayerSide playerSide;

    Piece(PlayerSide playerSide) {
        this.playerSide = playerSide;
    }
    public PlayerSide getPlayerSide() {
        return playerSide;
    }
    public abstract Image getImage();
    public abstract ArrayList<Case> getPotentialMove(Case currentCase, Board board);
}
