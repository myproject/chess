package model;

import game.Cache;

import java.awt.*;

public class Case {
    Vector position;
    Piece piece;
    Color color;

    public Case(Vector position, Piece piece, Color color) {
        this.position = position;
        this.piece = piece;
        this.color = color;
    }

    public Case(Vector position, Color color) {
        this.position = position;
        this.piece = null;
        this.color = color;
    }

    public Vector getPosition() {
        return position;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public Color getColor() {
        return color;
    }

    public boolean pointInCase(Point point) {
        return point.x >= position.getX() * Cache.TAILLECASE && point.x <= position.getX() * Cache.TAILLECASE + Cache.TAILLECASE  && point.y >= position.getY() * Cache.TAILLECASE + 115 && point.y <= position.getY() * Cache.TAILLECASE + Cache.TAILLECASE + 115;
    }
}
