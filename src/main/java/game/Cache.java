package game;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Cache {
    public static JFrame fenetre;
    public static boolean run = true;

    // settings
    public final static int TAILLECASE = 62;

    //Images
    public static Image cavalierBlanc;
    public static Image cavalierNoir;
    public static Image dameBlanc;
    public static Image dameNoir;
    public static Image fouBlanc;
    public static Image fouNoir;
    public static Image pionBlanc;
    public static Image pionNoir;
    public static Image roiBlanc;
    public static Image roiNoir;
    public static Image tourBlanc;
    public static Image tourNoir;

    public static void init() {
        try {
            cavalierBlanc = ImageIO.read(Cache.class.getResource("/cavalier_blanc.png"));
            cavalierNoir = ImageIO.read(Cache.class.getResource("/cavalier_noir.png"));
            dameBlanc = ImageIO.read(Cache.class.getResource("/dame_blanc.png"));
            dameNoir = ImageIO.read(Cache.class.getResource("/dame_noir.png"));
            fouBlanc = ImageIO.read(Cache.class.getResource("/fou_blanc.png"));
            fouNoir = ImageIO.read(Cache.class.getResource("/fou_noir.png"));
            pionBlanc = ImageIO.read(Cache.class.getResource("/pion_blanc.png"));
            pionNoir = ImageIO.read(Cache.class.getResource("/pion_noir.png"));
            roiBlanc = ImageIO.read(Cache.class.getResource("/roi_blanc.png"));
            roiNoir = ImageIO.read(Cache.class.getResource("/roi_noir.png"));
            tourBlanc = ImageIO.read(Cache.class.getResource("/tour_blanc.png"));
            tourNoir = ImageIO.read(Cache.class.getResource("/tour_noir.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
