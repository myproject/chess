package game;

import model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GamePlay extends JPanel implements KeyListener, ActionListener, MouseListener
{
    private Board board;
    private Case selectedCase;
    private PlayerSide turn = PlayerSide.white;
    private boolean isEnd = false;
    private Case upgradePion;

    public GamePlay()
    {
        addMouseListener(this);
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }

    public void newBoard()
    {
        board = new Board();
    }

    @Override
    public void paint(Graphics g)
    {
        // fill background
        g.setColor(new Color(0, 0, 0));
        g.fillRect(0, 0, Cache.fenetre.getWidth(), Cache.fenetre.getHeight());

        // player turn text
        g.setColor(Color.white);
        g.setFont(new Font("TimesRoman", Font.PLAIN, 30));
        g.drawString("Player turn: " + (turn == PlayerSide.white ? "White" : "Black"), 125, 52);

        // draw all case
        for(Case c: board.getCases()) {
            Vector reelPosition = new Vector(c.getPosition().getIntX() * Cache.TAILLECASE, c.getPosition().getIntY() * Cache.TAILLECASE + 85);

            // fill color case
            g.setColor(c.getColor());
            g.fillRect(reelPosition.getIntX(), reelPosition.getIntY(), Cache.TAILLECASE, Cache.TAILLECASE);

            // draw image of piece
            if(c.getPiece() != null) {
                g.drawImage(c.getPiece().getImage(), reelPosition.getIntX(), reelPosition.getIntY(), Cache.TAILLECASE, Cache.TAILLECASE, null);
            }
        }

        // draw circle for move a piece
        if(selectedCase != null) {
            if(selectedCase.getPiece() != null) {
                for(Case canMove : selectedCase.getPiece().getPotentialMove(selectedCase, board)) {
                    g.setColor(new Color(0, 0, 0, (float) 0.6));
                    g.fillRoundRect(canMove.getPosition().getIntX() * Cache.TAILLECASE + Cache.TAILLECASE / 6, canMove.getPosition().getIntY() * Cache.TAILLECASE + Cache.TAILLECASE + Cache.TAILLECASE / 2, Cache.TAILLECASE / 3 * 2, Cache.TAILLECASE / 3 * 2, 45, 45);
                }
            }
        }

        // draw end information
        if(isEnd) {
            g.setColor(Color.BLACK);
            g.fillRect(0, 200, Cache.fenetre.getWidth(), (Cache.fenetre.getHeight() - 200) / 2);
            g.setColor(Color.white);
            g.drawString("Un joueur à gagné la partie", 60, (Cache.fenetre.getHeight() - 200) / 2 + 100);
            g.drawString("f5 to restart", 175, (Cache.fenetre.getHeight() - 200) / 2 + 150);
        }

        // draw upgrade pion informations
        if(upgradePion != null) {
            g.setColor(Color.BLACK);
            g.fillRect(0, 200, Cache.fenetre.getWidth(), (Cache.fenetre.getHeight() - 200) / 2);
            g.setColor(Color.white);
            g.setFont(new Font("TimesRoman", Font.PLAIN, 20));
            g.drawString("Appuyer sur une des touches suivantes", 75, (Cache.fenetre.getHeight() - 200) / 2 + 70);
            g.drawString("pour améliorer votre pion", 125, (Cache.fenetre.getHeight() - 200) / 2 + 100);
            g.drawString("F: fou    D: dame    T: tour    C: cavalier", 75, (Cache.fenetre.getHeight() - 200) / 2 + 150);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {}

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e)
    {
        if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
        {
            Cache.run = false;
        } else if(e.getKeyCode() == KeyEvent.VK_F5 && isEnd) {
            newBoard();
            turn = PlayerSide.white;
            isEnd = false;
        } else if(e.getKeyCode() == KeyEvent.VK_F && upgradePion != null) {
            upgradePion.setPiece(new Fou(upgradePion.getPiece().getPlayerSide()));
            upgradePion = null;
        } else if(e.getKeyCode() == KeyEvent.VK_D && upgradePion != null) {
            upgradePion.setPiece(new Dame(upgradePion.getPiece().getPlayerSide()));
            upgradePion = null;
        } else if(e.getKeyCode() == KeyEvent.VK_C && upgradePion != null) {
            upgradePion.setPiece(new Cavalier(upgradePion.getPiece().getPlayerSide()));
            upgradePion = null;
        } else if(e.getKeyCode() == KeyEvent.VK_T && upgradePion != null) {
            upgradePion.setPiece(new Tour(upgradePion.getPiece().getPlayerSide()));
            upgradePion = null;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {}

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {
        if(e.getButton() == MouseEvent.BUTTON1 && !isEnd && upgradePion == null) {
            Point mousePosition = Cache.fenetre.getMousePosition();

            // check if mouse are in windows
            if(mousePosition != null) {
                // get case at mouse position
                Case mouseAt = board.getCaseByPosition(mousePosition);

                // if mouse position are in case
                if(mouseAt != null) {
                    // if a case is already selected
                    if(selectedCase != null) {
                        // if player click on potential move case of piece, move piece
                        if(selectedCase.getPiece().getPotentialMove(selectedCase, board).contains(mouseAt)) {

                            // if piece is a Pion, taken in passing and upgrade
                            if(selectedCase.getPiece() instanceof Pion) {
                                // taken in passing
                                int distance;
                                // get distance beetwen pion and future place of piece
                                if(selectedCase.getPiece().getPlayerSide() == PlayerSide.white) {
                                    distance = mouseAt.getPosition().getIntY() - selectedCase.getPosition().getIntY();
                                } else {
                                    distance = selectedCase.getPosition().getIntY() - mouseAt.getPosition().getIntY();
                                }

                                // move pion
                                ((Pion) selectedCase.getPiece()).play(distance == 2 || distance == -2, selectedCase, mouseAt, board);

                                // upgrade
                                if(mouseAt.getPosition().getIntY() == 0 && selectedCase.getPiece().getPlayerSide() == PlayerSide.white || mouseAt.getPosition().getIntY() == 7 && selectedCase.getPiece().getPlayerSide() == PlayerSide.black) {
                                    upgradePion = mouseAt;
                                }
                            }

                            mouseAt.setPiece(selectedCase.getPiece());

                            board.resetPriseEnPassant(turn == PlayerSide.white ? PlayerSide.black : PlayerSide.white);
                            selectedCase.setPiece(null);
                            selectedCase = null;
                            turn = turn == PlayerSide.white ? PlayerSide.black : PlayerSide.white;
                            if(checkEndGame()) {
                                isEnd = true;
                            }
                        // player click on case where piece cannot go
                        } else {
                            // set selected case at mouse if player click on piece else unselect piece
                            if(mouseAt.getPiece() != null && mouseAt.getPiece().getPlayerSide() == turn) {
                                selectedCase = mouseAt;
                            } else {
                                selectedCase = null;
                            }
                        }
                    // no case already selected
                    } else {
                        // set selectedcase at case of mouse if player click on piece
                        if(mouseAt.getPiece() != null && mouseAt.getPiece().getPlayerSide() == turn) {
                            selectedCase = board.getCaseByPosition(mousePosition);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    private boolean checkEndGame() {
        return board.getCases()
                .stream()
                .filter(aCase -> aCase.getPiece() != null && aCase.getPiece() instanceof Roi)
                .count() != 2;

    }
}
