package game;

import model.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class Board {

    ArrayList<Case> cases;
    public Board() {
        cases = new ArrayList<>();

        // initialise game with cases and piece on him
        for(int x = 0; x < 8; ++x) {
            for(int y = 0; y < 8; ++y) {

                Vector position = new Vector(x, y);
                Color color = x % 2 + y % 2 == 1 ? new Color(113, 140, 81) : Color.white;

                if(y == 1 || y == 6) {
                    PlayerSide side = y == 1 ? PlayerSide.black : PlayerSide.white;
                    cases.add(new Case(position, new Pion(side), color));
                } else if(y == 0 || y == 7) {
                    PlayerSide side = y == 0 ? PlayerSide.black : PlayerSide.white;

                    if(x == 0 || x == 7) {
                        cases.add(new Case(position, new Tour(side), color));
                    } else if(x == 1 || x == 6) {
                        cases.add(new Case(position, new Cavalier(side), color));
                    } else if(x == 2 || x == 5) {
                        cases.add(new Case(position, new Fou(side), color));
                    } else if(x == 3) {
                        cases.add(new Case(position, new Dame(side), color));
                    } else {
                        cases.add(new Case(position, new Roi(side), color));
                    }
                } else {
                    cases.add(new Case(position, color));
                }
            }
        }
    }

    public ArrayList<Case> getCases() {
        return cases;
    }

    public Case getCaseByPosition(Point position) {
        Case selectedCase = null;

        for(Case c : cases) {
            if(c.pointInCase(position)) {
                selectedCase = c;
            }
        }

        return selectedCase;
    }

    public Case getCaseAtCoordinate(Vector position) {
        return cases
                .stream()
                .filter(aCase -> aCase.getPosition().getX() == position.getX() && aCase.getPosition().getY() == position.getY())
                .findAny()
                .orElse(null);
    }

    public void resetPriseEnPassant(PlayerSide playerSide) {
         cases
                .stream()
                .filter(aCase -> aCase.getPiece() != null && aCase.getPiece().getPlayerSide() == playerSide && aCase.getPiece() instanceof Pion)
                 .forEach(aCase -> ((Pion) aCase.getPiece()).setCanPriseEnPassant(false));

    }
}

